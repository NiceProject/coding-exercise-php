$(document).ready(function() {
    var max_fields = 100;
    var wrapper = $(".paths_wrap");
    var add_button = $(".add_path_button");

    var x = 1;
    $(add_button).click(function(e){
        e.preventDefault();
        if(x < max_fields){
            x++;
            $(wrapper).append('<div><input type="text" name="paths[]"/><a href="#" class="remove_path">Remove</a></div>');
        }
    });
   
    $(wrapper).on("click",".remove_path", function(e){
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
