Given the below User Story, finish the application. There are no time constraints and you are free to use any resources at your disposal.

Please consider and exhibit Object-Oriented principles in your solution.

User Story:
As a user, I want to be able to check how much space is each of my special files taking.

Acceptance Criteria:
1. I can specify special files by adding their paths to the list
2. If I've added a new item to the list, the path in it must be specified
3. If the file contains the word "overhead" in it's path, than additional 4 bytes must be added to the file size
3. After specifying all of my paths I can click a button called "Count" and the size for each file will be shown, as well as a total of the files sizes

Additional Info:
The functionality of counting the available disk space will be later needed in another part of the application with additional information about the files. Also the counting strategy will probably change.

In case of any questions please don't hesitate to ask.

After completing the task create a bundle of the repository and send it back per email:
git bundle create surname_name.bundle master